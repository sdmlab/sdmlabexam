const express = require('express')
const cors=require('cors')
const movieRouter=require('./routes/movie')

const app = express();
app.use(cors('*'))
app.use(express.json())
//add the router
app.use('/movie', movieRouter)

app.listen(4000, '0.0.0.0', () => {
    console.log('server started successfully on port no 4000...!')
})

